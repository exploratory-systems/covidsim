import { Component } from '@angular/core';
import { AppstateService } from 'src/app/services/appstate.service';

@Component({
  selector: 'app-side-toggle',
  template: `
  <button class="toggle" mat-button (click)="toggle()">
    <mat-icon aria-hidden="false" aria-label="control menu icon">menu</mat-icon>
  </button>`,
  styles: [`.toggle { min-width: 48px; padding: 0px; }`]
})
export class SideToggleComponent {

  constructor(private appstateService: AppstateService) {}

  toggle = () => this.appstateService.toggle('Side');

}

