import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { Dataset } from 'src/app/model/simulation-result';
import { NvD3Component } from 'ng2-nvd3';

@Component({
  selector: 'app-overview-chart',
  template: '<nvd3 class="nvd3" #nvd3 [options]="multiChart" [data]="data"></nvd3>',
  styleUrls: ['../../../../node_modules/nvd3/build/nv.d3.css'],
  encapsulation: ViewEncapsulation.None
})
export class OverviewChartComponent implements OnInit {

  @Input() data: Dataset[];

  multiChart = {
    chart: {
      type: 'multiChart',
      // width: 800,
      height: 300,
      margin : { top: 40, right: 50, bottom: 50, left: 85 },
      x(d) { return d.x; },
      y(d) { return d.y; },
      legend: {
        align: false,
      },
      interactiveLayer: {
        tooltip: {
          valueFormatter: (d, i) =>
          (i > 3) ? d3.format(',.3f')(d) : d3.format(',.0f')(d)
        }
      },
      showValues: true,
      focusEnable: true,
      useInteractiveGuideline: true,
      legendRightAxisHint: '',
      duration: 0,
      xAxis: { axisLabel: 'Day' },
      yAxis1: {
        axisLabel: 'Individuals',
        axisLabelDistance: 25,
        tickFormat: (d) => d3.format(',.0f')(d)
      },
      yAxis2: {
        axisLabel: 'Detection Probability / Contact Reduction',
        axisLabelDistance: -30,
      }
    }
  };

  constructor() { }

  ngOnInit() { }

}
