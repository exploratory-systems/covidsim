import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sensitivity',
  templateUrl: './sensitivity.component.html',
  styleUrls: ['./sensitivity.component.scss']
})
export class SensitivityComponent implements OnInit {

  lineChart = {
    chart: {
      type: 'lineChart',
      height: 300,
      margin : { top: 20, right: 20, bottom: 50, left: 85 },
      x(d) { return d.x; },
      y(d) { return d.y; },
      showValues: true,
      useInteractiveGuideline: true,
      valueFormat(d) { return d3.format(',.4f')(d); },
      duration: 500,
      xAxis: { axisLabel: 'Day' },
      yAxis: { axisLabel: 'Individuals', axisLabelDistance: 15 }
    }
  };

  constructor() { }

  ngOnInit() {
  }

}
