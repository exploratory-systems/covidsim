import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs';
import { ModelService } from 'src/app/services/model.service';
import { NvD3Component } from 'ng2-nvd3';
import { PARAMETER_PROPERTIES } from 'src/app/model/parameter-properties';
import { SimulationResult } from 'src/app/model/simulation-result';

declare let d3: any;

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: [
    '../../../../node_modules/nvd3/build/nv.d3.css',
    './content.component.scss'
  ],

})
export class ContentComponent implements OnInit {

  blue = { borderColor: 'blue' };
  red = { borderColor: 'red' };
  green = { borderColor: 'green' };
  black = { borderColor: 'black' };
  pink = { borderColor: 'pink' };

  days;
  legend;
  colors = [ this.blue, this.red, this.green, this.black, this.pink ];

  sparkLine = {
    chart: {
      type: 'multiChart',
      height: 100,
      margin : { top: 30, right: 50, bottom: 20, left: 85 },
      x(d) { return d.x; },
      y(d) { return d.y; },
      showLegend: false,
      useInteractiveGuideline: true,
      interactiveLayer: {
        tooltip: {
          valueFormatter: (d) => d3.format(',.1f')(d)
        }
      },
      xAxis: { ticks: null },
      yAxis1: { axisLabel: 'R\u2080', ticks: null },
      yAxis2: {
        axisLabel: 'R\u2080',
        axisLabelDistance: 0,
        rotateYLabel: false,
        ticks: null }
    }
  };


  data: SimulationResult;
  data0;
  data1;
  data2;
  data3;
  data4;
  data5;
  data6 = [];

  options = [
    { label: 'Cumulative Incidence', data: this.data3 },
    { label: 'Daily Incidence', data: this.data4 },
    { label: 'Weekly Incidence', data: this.data5 },
  ];


  incidence = this.options[0];

  parameter;
  parameterKeys;
  key = 'basic_reproduction_number';
  minValue = 2;
  maxValue = 4;
  steps = 4;

  r0 = false;

  chart;
  testdata;

  subscription: Subscription;

  ngOnInit(): void {
  }

  constructor( public model: ModelService ) {
    this.parameter = PARAMETER_PROPERTIES;
    this.parameterKeys = ['basic_reproduction_number'];

    this.subscription = model.result()
    .subscribe( data => {

      if (data) {
        if (data.update < 0) {
          this.update(data);
        } else {
          this.series(data);
        }
      } else {
        console.log('null result');
        this.data0 = [];
        this.data1 = [];
        this.data2 = [];
        this.data3 = [];
        this.data4 = [];
        this.data5 = [];
        this.incidence.data = [];
      }

    });
  }

  update(data: SimulationResult) {
    if (this.data) {
      // console.log('data', data, Object.keys(data));
      Object.keys(data).forEach( key => {
        if (key !== 'update') {
          this.data[key].values = [...data[key].values];
        }
      });
    } else {
      this.data = data;
    }
    if (this.data) {
      const d = this.data;
      this.data0 = [ d.r0 ];
      this.data1 = [ d.susceptible, d.infected, d.recovered, d.dead, d.detection, d.contactReduction ];
      this.data2 = [ d.latent, d.prodromal, d.asymptomatic, d.sick,
                     d.hospitalized, d.icu, d.isolated, d.secluded ];
      this.options[0].data = [ d.CIinfections, d.CIsick, d.CIoutpatients,
                     d.CIhospitalisations, d.CIicu, d.CIdeaths ];
      this.options[1].data = [ d.DIinfections, d.DIsick, d.DIoutpatients,
                     d.DIhospitalisations, d.DIicu, d.DIdeaths ];
      this.options[2].data = [ d.WIinfections, d.WIsick, d.WIoutpatients,
                     d.WIhospitalisations, d.WIicu, d.WIdeaths ];
    }
  }

  series(data) {
    console.log('series---', data[1]);
    const index = data[0].update;
    const series = data[0].series;
    const value = data[0].value;
    console.log('series', index, series, value, data);

    if (this.data6[index]) {
      this.data6[index].values = data[1].values;
    } else {
      this.data6[index] = data[1];
    }

    this.data6 = [...this.data6];

    this.data6[index].area = false;

    this.data6[index].key = value;

    console.log('data6', this.data6);

  }
}
