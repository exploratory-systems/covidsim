import { Component, Input, OnInit } from '@angular/core';
import { ModelService } from '../../services/model.service';
import { ParameterProperties } from 'src/app/model/parameter-properties';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {

  @Input() parameter: ParameterProperties;

  field: FormControl;

  constructor(private model: ModelService) { }


  ngOnInit() {
    this.field = new FormControl(this.parameter.default, [
      Validators.required,
      Validators.min(this.parameter.min),
      Validators.max(this.parameter.max)
    ]);
  }

  doInput(value: number) {
    // console.log('input', value);
    // this.parameter.default = value;
    this.field.setValue(value);
  }

  change(value) {
    console.log('change', value);
    if (value < this.parameter.min) { value = this.parameter.min; }
    if (value > this.parameter.max) { value = this.parameter.max; }
    this.parameter.default = value;
    this.field.setValue(value);
    this.model.update( this.parameter.key, +value );
  }

  keydown(event) {
    // console.log('keydown', event);
    if (event.keyCode === 13) {
      // console.log('return', event);
      this.change(event.srcElement.valueAsNumber);
      this.click(event);
    }
  }

  click(event) {
    // console.log('click', event);
    event.preventDefault();
    event.stopPropagation();
  }

  blur(value) {
    // console.log('blur', value);
    this.change(value);
  }

}
