import { Component, OnInit } from '@angular/core';
import { ModelService } from 'src/app/services/model.service';
import { PARAMETER_PROPERTIES } from 'src/app/model/parameter-properties';

@Component({
  selector: 'app-control',
  templateUrl: './control.component.html',
  styleUrls: ['./control.component.css']
})
export class ControlComponent implements OnInit {

  parameter;

  constructor(
    public model: ModelService
  ) {
    this.parameter = PARAMETER_PROPERTIES;
  }

  ngOnInit() {
  }

}
