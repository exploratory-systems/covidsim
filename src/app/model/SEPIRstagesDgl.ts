import { Dgl } from './runge-kutta-solver';
import { ParameterSettings } from './parameter-settings';
import { Population } from './population';

const SINUSFACTOR = 2 * Math.PI / 365;

interface Flow {
  source: number;
  target: number;
  rate: number;
  counter?: number;
}

export class SEPIRstagesDgl implements Dgl {

  flows: Flow[];

  settings = {
    contactReduction: 1,
    isolationCapacity: 0
  };

  popsize: number;
  prodromalContagiousness: number;
  lateInfectiveContagiousness: number;
  homeIsolationContagiousness: number;
  symptomaticFraction: number;
  isolatedFraction: number;

  externalFoI: number;

  maxSeasonalTransmissionFactor: number;
  maxSeasonalTransmissionDay: number;

  rateSE: number;

  population: Population;

  sum = (a: number, b: number) => a + b;

  init( params: ParameterSettings ): number[] {

    this.population = new Population(params);

    this.popsize = params.population_size * 1000000;
    this.externalFoI = params.external_force_of_infection / this.popsize;

    this.maxSeasonalTransmissionFactor = params.maximal_seasonal_transmission_factor / 100;
    this.maxSeasonalTransmissionDay = params.maximal_seasonal_transmission_day;

    this.prodromalContagiousness = params.prodromal_contagiousness / 100;
    this.lateInfectiveContagiousness = params.late_infective_contagiousness / 100;
    this.homeIsolationContagiousness = 1 - params.contact_reduction_home_isolation / 100;

    this.symptomaticFraction = params.symptomatic_course_of_disease / 100;
    this.isolatedFraction = params.symptomatic_cases_to_be_isolated / 100;

    this.rateSE = params.basic_reproduction_number /
                ( params.prodromal_duration * this.prodromalContagiousness
                + params.early_infective_duration
                + params.late_infective_duration * this.lateInfectiveContagiousness);

    const rateEP = params.erlang_stages / params.latency_duration;
    const ratePI = params.erlang_stages / params.prodromal_duration;
    const rateIL = params.erlang_stages / params.early_infective_duration;
    const rateLR = params.erlang_stages / params.late_infective_duration;

    // console.log(this.rateSE, rateEP, ratePI, rateIC, rateCR);

    // console.log('rates', this.rateIC, this.rateCR);

    const doProdromalPeriod = params.prodromal_duration > 0;
    const doLateInfectivePeriod = params.late_infective_duration > 0;

    const E = (stage: number) => this.population.offset.E + stage;
    const P = (stage: number) => this.population.offset.P + stage;
    const I = (stage: number) => this.population.offset.I + stage;
    const L = (stage: number) => this.population.offset.L + stage;
    const R = () => this.population.offset.R;

    this.flows = [];

    for (let i = 1; i < params.erlang_stages; i++) {
      this.flows.push({ source: E(i - 1), target: E(i), rate: rateEP });
    }

    if (doProdromalPeriod) {
      this.flows.push({ source: E( params.erlang_stages - 1), target: P(0), rate: rateEP });

      for (let i = 1; i < params.erlang_stages; i++) {
        this.flows.push({source: P(i - 1), target: P(i), rate: ratePI });
      }

      this.flows.push({ source: P(params.erlang_stages - 1), target: I(0), rate: ratePI,
        counter: this.population.offset.CIC });

    } else {

      this.flows.push({ source: E(params.erlang_stages - 1), target: I(0), rate: rateEP });

    }

    for (let i = 1; i < params.erlang_stages; i++) {
      this.flows.push({ source: I(i - 1), target: I(i), rate: rateIL });
    }

    if (doLateInfectivePeriod) {

      this.flows.push({ source: I(params.erlang_stages - 1), target: L(0), rate: rateIL });

      for (let i = 1; i < params.erlang_stages; i++) {
        this.flows.push({ source: L(i - 1), target: L(i), rate: rateLR });
      }

      this.flows.push({ source: L(params.erlang_stages - 1), target: R(), rate: rateLR });

    } else {

      this.flows.push({ source: I(params.erlang_stages - 1), target: R(), rate: rateIL });

    }

    const susceptibles = this.popsize - params.initial_cases;
    const out = new Array<number>(this.population.equations()).fill(0);
    out[this.population.offset.S] = susceptibles;
    out[this.population.offset.E] = params.initial_cases;

    // console.log('Initial Population', out);

    return out;
  }

  set(key: string, value: number | boolean) {
    this.settings[key] = value;
  }

  eval(x: number, y: number[], dy: number[]) {

    dy.fill(0);

    const offset = this.population.offset;

    const prodromalCases      = this.population.prodromal(y);
    const earlyInfectiveCases = this.population.earlyInfective(y);
    const lateInfectiveCases  = this.population.lateInfective(y);
    const infectiveCases = earlyInfectiveCases + lateInfectiveCases;
    const detectedCases = infectiveCases * this.symptomaticFraction * this.isolatedFraction;

    const isolatedCases = (this.settings.isolationCapacity < 0) ? 0 :
    Math.min(detectedCases, this.settings.isolationCapacity);
    const isolatedEarlyInfectiveCases = (infectiveCases === 0) ? 0 : isolatedCases * earlyInfectiveCases / infectiveCases;
    const isolatedLateInfectiveCases = isolatedCases - isolatedEarlyInfectiveCases;

    const homeIsolatedCases = (this.settings.isolationCapacity < 0) ? 0 : detectedCases - isolatedCases;
    const homeIsolatedEarlyInfectiveCases = (detectedCases === 0) ? 0 : homeIsolatedCases * earlyInfectiveCases / infectiveCases;
    const homeIsolatedLateInfectiveCases = homeIsolatedCases - homeIsolatedEarlyInfectiveCases;

    const FoI = ( prodromalCases * this.prodromalContagiousness
      + homeIsolatedEarlyInfectiveCases * this.homeIsolationContagiousness
      + earlyInfectiveCases - isolatedEarlyInfectiveCases - homeIsolatedEarlyInfectiveCases
      + homeIsolatedLateInfectiveCases * this.homeIsolationContagiousness * this.lateInfectiveContagiousness
      + (lateInfectiveCases - isolatedLateInfectiveCases - homeIsolatedLateInfectiveCases) * this.lateInfectiveContagiousness
    ) / this.popsize;

    const currentSeasonalFactor = 1 + this.maxSeasonalTransmissionFactor
         * Math.cos(SINUSFACTOR * ( x - this.maxSeasonalTransmissionDay ));

    const rate = this.rateSE * FoI * currentSeasonalFactor * this.settings.contactReduction + this.externalFoI;

    const doFlow =  (flow: Flow) => {
      const f =  y[flow.source] * flow.rate;
      dy[flow.source] -= f;
      dy[flow.target] += f;
      if (flow.counter) { dy[flow.counter] += f; }
    };

    doFlow({ source: offset.S, target: offset.E, rate, counter: offset.CII });

    this.flows.forEach( doFlow );

    if (x < 0) {
      console.log('Population on day', x, y, dy);
    }
  }
}
