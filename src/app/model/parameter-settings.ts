export interface ParameterSettings {

  population_size: number;
  initial_cases: number;
  external_force_of_infection: number;
  simulation_days: number;

  latency_duration: number;
  prodromal_duration: number;
  early_infective_duration: number;
  late_infective_duration: number;

  hospitalisation_duration: number;
  icu_duration: number;

  // latency_stages: number;
  // prodromal_stages: number;
  // infective_stages: number;
  // declining_stages: number;
  erlang_stages: number;

  symptomatic_course_of_disease: number;
  medical_consultations_by_symptomatic_cases: number;
  hospitalisation_of_symptomatic_cases: number;
  icu_patients_among_hospitalized_cases: number;
  case_fatality_of_symptomatic_cases: number;

  basic_reproduction_number: number;
  maximal_seasonal_transmission_factor: number;
  maximal_seasonal_transmission_day: number;
  prodromal_contagiousness: number;
  late_infective_contagiousness: number;

  symptomatic_cases_to_be_isolated: number;
  case_isolation_capacity: number;
  contact_reduction_home_isolation: number;
  case_isolation_begin: number;
  case_isolation_range: number;

  contact_reduction_factor: number;
  contact_reduction_begin: number;
  contact_reduction_range: number;

  sick_threshold: number;
  sick_triggered_contact_reduction: number;
  hospitalisation_threshold: number;
  hospitalisation_triggered_contact_reduction: number;
  icu_threshold: number;
  icu_triggered_contact_reduction: number;

  detection_probability_medical_consultation: number;
  detection_probability_hospitalisation: number;
  detection_probability_deceased_case: number;
}
