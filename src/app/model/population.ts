import { ParameterSettings } from './parameter-settings';

const sum = (a: number, b: number) => a + b;

export class Population {

  offset;

  stages: number;

  params: ParameterSettings;

  numberOfEquations: number;

  constructor( params: ParameterSettings ) {

    this.params = params;

    const stages = params.erlang_stages;

    const S = 0;
    const E = S + 1;
    const P = E + stages;
    const I = P + stages;
    const L = I + stages;
    const R = L + stages;

    const CII = R + 1;
    const CIC = CII + 1;

    const Isolated = CIC + 1;
    const Secluded = Isolated + 1;

    this.numberOfEquations = Secluded + 1;
    this.offset = { S, E, P, I, L, R, CII, CIC, Isolated, Secluded };

    this.stages = stages;
  }

  equations() {
    return this.numberOfEquations;
  }

  susceptible(y: number[]): number {
    return y[this.offset.S];
  }

  latent(y: number[]): number {
    return y.slice(this.offset.E, this.offset.E + this.stages).reduce(sum, 0);
  }

  prodromal(y: number[]): number {
    return y.slice(this.offset.P, this.offset.P + this.stages).reduce(sum, 0);
  }

  earlyInfective(y: number[]): number {
    return y.slice(this.offset.I, this.offset.I + this.stages).reduce(sum, 0);
  }

  lateInfective(y: number[]): number {
    return y.slice(this.offset.L, this.offset.L + this.stages).reduce(sum, 0);
  }

  removed(y: number[]): number {
    return y[this.offset.R];
  }

  cumulativeInfections(y: number[]): number {
    return y[this.offset.CII];
  }

  cumulativeCases(y: number[]): number {
    return y[this.offset.CIC];
  }

  isolated(y: number[]): number {
    return y[this.offset.Isolated];
  }

  homeIsolated(y: number[]): number {
    return y[this.offset.Secluded];
  }

}
