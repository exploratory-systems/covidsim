import { Dgl } from './runge-kutta-solver';

const [S, E, P, I, R] = [0, 1, 2, 3, 4];

export class SEPIRdgl implements Dgl {

  settings = {};

  popsize: number;
  proCon: number;
  rateSE: number;
  rateEP: number;
  ratePI: number;
  rateIR: number;


  init( params ): number[] {

    this.popsize = params.population_size * 1000000;

    this.proCon = params.prodromal_contagiousness / 100;

    this.rateSE = params.basic_reproduction_number
                / ( params.prodromal_duration * this.proCon + params.infective_duration);

    this.rateEP = (1 / params.latency_duration);
    this.ratePI = (1 / params.prodromal_duration);
    this.rateIR = (1 / params.infective_duration);

    const susceptibles = this.popsize - params.initial_cases;
    return [ susceptibles, 0, 0, params.initial_cases, 0 ];
  }

  set(key: string, value: number | boolean) {
    this.settings[key] = value;
  }

  eval(x: number, y: number[], dy: number[]) {

    const FoI = (y[I] + this.proCon * y[P]) / this.popsize;

    const flowSE = this.rateSE * y[S] * FoI;
    const flowEP = this.rateEP * y[E];
    const flowPI = this.ratePI * y[P];
    const flowIR = this.rateIR * y[I];

    dy[S] =        - flowSE;
    dy[E] = flowSE - flowEP;
    dy[P] = flowEP - flowPI;
    dy[I] = flowPI - flowIR;
    dy[R] = flowIR;
  }
}
