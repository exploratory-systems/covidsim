import { Injectable, OnDestroy } from '@angular/core';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { fromWorkerPool } from 'observable-webworker';
import { saveAs } from 'file-saver';


import { ParameterSettings } from '../model/parameter-settings';
import { SimulationRequest } from '../model/simulation-request';
import { PARAMETER_PROPERTIES } from '../model/parameter-properties';
import { SimulationResult } from '../model/simulation-result';

@Injectable({
  providedIn: 'root'
})
export class ModelService implements OnDestroy {

  dataset = new BehaviorSubject<SimulationResult>(null);

  parameters: ParameterSettings;
  simulationRequest = new Subject<SimulationRequest>();

  constructor() {
    fromWorkerPool<any, any>( () =>
    new Worker('../model/simulation.worker', { type: 'module' }),
    this.simulationRequest.asObservable())
    .subscribe( (data) => this.dataset.next(data));

    this.reset();
  }

  reset() {
    this.parameters = Object.assign({},
      ...Object.entries(PARAMETER_PROPERTIES).map(([k, v]) => ({[k]: v.default})));
    this.simulationRequest.next({ parameters: this.parameters, update: -1 });
  }

  update( key: string, value: number ) {
    if (this.parameters[key] !== value) {
      this.parameters[key] = value;
      this.simulationRequest.next({ parameters: this.parameters, update: -1} );
    }
  }

  series( key: string, minValue: number, maxValue: number, steps: number) {
    const delta = (maxValue - minValue) / steps;
    for (let i = 0; i <= steps; i++) {
       this.simulationRequest.next({
        parameters: this.parameters,
        update: i,
        parameterKey: key,
        parameterValue: minValue + i * delta
       });
    }
  }

  result(): Observable<SimulationResult> {
    return this.dataset.asObservable();
  }

  ngOnDestroy(): void {
    this.simulationRequest.complete();
  }

  save() {
    const data = this.dataset.getValue();

    const { WIinfections, WIsick, WIoutpatients, WIhospitalisations, WIicu, WIdeaths,
      update, ...filteredData } = data;

    const headers = [ 'Day', ...Object.keys(filteredData).map(d => data[d].label)].join(', ');

    const rows = [];
    for (let day = 0; day <= this.parameters.simulation_days; day++) {
      const row = [ day, ...Object.keys(filteredData).map(d => data[d].values[day].y)].join(', ');
      rows.push( row );
    }

    const results = [ headers, ...rows ].join('\n');

    const blob = new Blob([results], { type: 'text/comma-separated-values'} );

    saveAs( blob, 'CovidSIM-results.csv');
  }

}
