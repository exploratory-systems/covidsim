import { Injectable } from '@angular/core';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

interface Appstate {
  Side: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class AppstateService {

  initialState = {
    Side: true
  };

  appstate = new BehaviorSubject<Appstate>( this.initialState );

  constructor() { }

  of(key: string): Observable<any> {
    return this.appstate.pipe(map(x => x[key]));
  }

  toggle( key: string ): void {
    this.appstate
    .next( {...this.appstate.value, [key]: !this.appstate.value[key]} );
  }
}
